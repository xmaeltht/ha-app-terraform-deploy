terraform {
  backend "s3" {
    bucket         = "mkloud-terrastate-bucket"
    key            = "ha-app-project_1/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}